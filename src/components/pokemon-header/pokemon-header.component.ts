import { Component, EventEmitter, Input, Output, OnInit } from '@angular/core';
import { Results } from 'src/interfaces';



@Component({
  selector: 'app-pokemon-header',
  templateUrl: './pokemon-header.component.html',
  styleUrls: ['./pokemon-header.component.scss']
})
export class PokemonHeaderComponent implements OnInit {
  @Output() searchChange = new EventEmitter();
  @Output() typeSelected = new EventEmitter();

  types: Array<string>;
  pokemonList: Array<Results>;
  search: string;
  currentType: string;
  currentAbilities: Array<string>;


  @Input() set pokemons(pokemons: Results[]) {
    if (pokemons !== this.pokemonList) {
      this.pokemonList = pokemons;

      // Get types and abilities from each pokemon
      this.pokemonList.forEach(pokemon => {
        this.setPokemonTypes(pokemon);
      });
    }
  }

  ngOnInit(): void {
    this.types = [];
  }

  /**
   * Called when a search field is updated
   */
  searchEvent(search?): void {
    // check for cleared search
    if (search === '') {
      this.search = search;
    }
    this.searchChange.emit(this.search);
  }

  /**
   * Called when a type has been selected
   */
  onTypeSelected(): void {
    if (this.currentType) {
      this.typeSelected.emit(this.currentType);
    } else {
      this.typeSelected.emit('');
    }
  }

  /**
   * Grabs a pokemons types and adds to array
   */
  setPokemonTypes(pokemon: Results): void {
    if (pokemon) {
      pokemon.details.types.forEach(type => {
        const typeName = type.type.name;
        if (!this.types.includes(typeName)) {
          this.types.push(typeName);
          this.types.sort();
        }
      });
    }
  }
}
