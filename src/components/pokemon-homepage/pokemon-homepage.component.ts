import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { PokeAPI, PokemonDetails, Results, TYPE_COLOURS } from 'src/interfaces';
import { PokemonService } from 'src/services/pokemon.service';

@Component({
  selector: 'app-pokemon-homepage',
  templateUrl: './pokemon-homepage.component.html',
  styleUrls: ['./pokemon-homepage.component.scss']
})
export class PokemonHomepageComponent implements OnInit {
  @Output() exportPokemons = new EventEmitter();
  @Output() pokemonClicked = new EventEmitter<Event>();
  pokemonsLoaded: boolean;
  pokemons: PokeAPI;
  query: string;

  typeFilters: string;
  offset: number = 0;
  details: PokeAPI;

  selectedPokemon: Results;

  @Input() set search(newSearch: string) {
    if (newSearch !== this.query) {
      this.query = newSearch;
    }
  }

  @Input() set typeFilter(type: string) {
    if (type !== this.typeFilter) {
      this.typeFilters = type;
    }
  }




  constructor(private pokemonService: PokemonService) {}

  ngOnInit(): void {
    this.pokemonsLoaded = false;
    this.getPokemons(this.offset);
  }

  onSelect(pokemons: Results): void {
    this.selectedPokemon = pokemons;
  }

  onPokemonClick(event: Event): void {
    this.pokemonClicked.emit(event);
  }

  onForwardClick(): void {
    this.offset = this.offset + 20;
    this.getPokemons(this.offset);
  }

  onPreviousClick(): void {
    if (this.offset !== 0) {
      this.offset = this.offset - 20;
      this.getPokemons(this.offset);
    }
  }


  /**
   * Loads in all 151 Original pokemon and gets
   * their details and species details
   */
  getPokemons(offset: number): void {
    this.pokemonService.getPokemon(offset).subscribe((data: PokeAPI) => {
      this.pokemons = data;

      if (this.pokemons.results && this.pokemons.results.length) {
        // get pokemon details for every pokemon
        this.pokemons.results.forEach(pokemon => {
          // set pokemon id
          pokemon.id = pokemon.url.split('/')[
            pokemon.url.split('/').length - 2
          ];

          this.getPokemonDetails(pokemon);
          this.getPokemonSpeciesDetails(pokemon);
        });
      }
    });
  }

  /**
   * Gets and sets a pokemons details
   */
  getPokemonDetails(pokemon: Results): void {
    this.pokemonService
      .getPokemonDetails(pokemon.name)
      .subscribe((details: PokemonDetails) => {
        pokemon.details = details;
        // when last pokemon details have been loaded
        // send pokemons to header component

          this.pokemonsLoaded = true;
          this.exportPokemons.emit(this.pokemons.results);

      });
  }

  /**
   * Gets and sets a species details
   * (currently only sets the description -
   * would be used when card is clicked and either
   * a new page/dialog with further information on
   * a pokemon is shown)
   */
  getPokemonSpeciesDetails(pokemon: Results): void {
    this.pokemonService
      .getPokemonSpecies(pokemon.name)
      .subscribe((species: any) => {
        const entries = species.flavor_text_entries;
        if (entries) {
          entries.some(flavor => {
            if (flavor.language.name === 'en') {
              pokemon.description = flavor.flavor_text;
            }
          });
        }
      });
  }

  /**
   * returns colour based on type mapped
   * in TYPE_COLOURS interface
   */
  _getTypeColour(type: string): string {
    if (type) {
      return '#' + TYPE_COLOURS[type];
    }
  }
}
